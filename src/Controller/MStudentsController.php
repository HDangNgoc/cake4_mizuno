<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * MStudents Controller
 *
 * @property \App\Model\Table\MStudentsTable $MStudents
 * @method \App\Model\Entity\MStudent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MStudentsController extends AppController
{   public $paginate = [
    'limit' => 3,
    ];
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {   $this->loadModel('MStudents');
        $student_no = $this->request->getQuery('student_no');
        $student_name = $this->request->getQuery('student_name');
        $student_name_phonetic = $this->request->getQuery('student_name_phonetic');
        $academy_name = $this->request->getQuery('academy_name');
        $enrollment_status = $this->request->getQuery('enrollment_status');
        $conditions=array();
        if($student_no) {$conditions[]=array('student_no LIKE'=>'%'.$student_no.'%');}
        if($student_name) {$conditions[]=array('student_name LIKE'=>'%'.$student_name.'%');}
        if($student_name_phonetic) {$conditions[]=array('student_name_phonetic LIKE'=>'%'.$student_name_phonetic.'%');}
        if($academy_name) {$conditions[]=array('academy_name'=>$academy_name);}
        if($enrollment_status) {$conditions[]=array('enrollment_status'=>$enrollment_status);}      
        if($this->request->is(['get']) ){
            $search = $this->MStudents->find('all')
                                        ->where(['AND'=>$conditions]);
        }
        else{
            $search = $this->MStudents;
        }
        $mStudents= $this->paginate($search);
        $this->set('search',$search);
        $this->set(compact('mStudents'));
    }

    /**
     * Edit method
     *
     * @param string|null $id M Student id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {   $this->loadModel('MStudents');
        $mStudent = $this->MStudents->get($id, [
            'contain' => [],
        ]);
        $student_password=$this->request->getData('student_password');
        $parent_password=$this->request->getData('parent_password');
        if($student_password==$parent_password) {
            if ($this->request->is(['patch', 'post', 'put'])) {
            $mStudent = $this->MStudents->patchEntity($mStudent, $this->request->getData());
            $errors = $mStudent->geterrors();
            if (empty($errors)) {
                $this->MStudents->save($mStudent);
                return $this->redirect(['action' => 'index']);
            } else {
                foreach ($errors as $key => $error) {
                    foreach ($error as $error_messages) {
                        $this->Flash->error($error_messages, ['key' => $key]);
                    }
                    unset($key, $error_messages);
                }
                unset($error);
            }
            }}
            else{$this->Flash->error(__('新しいパスワードと確認用パスワードが一致しません。'));}
        $this->set(compact('mStudent'));
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['login', 'index','add','confirm','view','delete','edit']);
    }
}
