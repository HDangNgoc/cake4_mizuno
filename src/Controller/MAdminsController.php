<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\Routing\Router;
use Cake\Auth\DefaultPasswordHasher;

/**
 * MAdmins Controller
 *
 * @property \App\Model\Table\MAdminsTable $MAdmins
 * @method \App\Model\Entity\MAdmin[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MAdminsController extends AppController
{   
    public function initialize():void
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {   $this->loadModel('MAdmins');
        $query =  $this->MAdmins->find()
        ->join([
        'table' => 'm_academy',
        'alias' => 'c',
        'type' => 'INNER',
        'conditions' => 'c.id = academy_id',
        ])
        ->select([
            'id' => 'MAdmins.id',
            'academy_name' => 'c.academy_name',
            'admin_name' => 'MAdmins.admin_name',
            'is_deleted'=> 'MAdmins.is_deleted',
            'admin_role'=> 'MAdmins.admin_role',
            'academy_id' => 'MAdmins.academy_id',
        ]);
        $key = $this->request->getQuery('key');
        $academy_id = $this->request->getQuery('academy_id');
        if($key && $academy_id){$search = $query->find('all')->where(['AND'=>['admin_name LIKE'=>'%'.$key.'%','academy_id'=>$academy_id]]);} 
        elseif ($key){$search = $query->find('all')->where(['OR'=>['admin_name LIKE'=>'%'.$key.'%']]);}
        elseif ($academy_id){$search = $query->find('all')->where(['OR'=>['academy_id'=>$academy_id]]);}
        else{
            $search = $query;
        }
        $query = $this->paginate($search);
        //$mAdmins = $this->paginate($this->MAdmins);
        $this->set('query',$query);
        $this->set('search',$search);
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add($id = null)
    {   $this->loadModel('MAdmins');
        if($id!=null) {$mAdmin = $this->MAdmins->get($id, ['contain' => [],]);
                if ($this->request->is(['patch', 'post', 'put'])) {
                $savedata = $this->request->getdata();
                $check = $this->MAdmins->newEntity($savedata);
                $errors = $check->geterrors();
                if (empty($errors)) { 
                $this->request->getsession()->write(['id'=>$this->request->getData('id'),
                                                    'academy_id'=>$this->request->getData('academy_id'),
                                                    'admin_name'=>$this->request->getData('admin_name'),
                                                    'admin_login_id'=>$this->request->getData('admin_login_id'),
                                                    'admin_password'=>$this->request->getData('admin_password'),
                                                    'admin_role'=>$this->request->getData('admin_role'),
                                                    ]);           
                $this->redirect(array('action' => 'confirm'));}
                else {
                    foreach ($errors as $key => $error) {
                        foreach ($error as $error_messages) {
                            $this->Flash->error($error_messages, ['key' => $key]);
                        }
                        unset($key, $error_messages);
                    }
                    unset($error);
                }}}
        else {$mAdmin = $this->MAdmins->newEmptyEntity();
                if ($this->request->is('post')) {
                    $savedata = $this->request->getdata();
                    $check = $this->MAdmins->newEntity($savedata);
                    $errors = $check->geterrors();
                if (empty($errors)) {
                $this->request->getsession()->write(['id'=>$this->request->getData('id'),
                                                    'academy_id'=>$this->request->getData('academy_id'),
                                                    'admin_name'=>$this->request->getData('admin_name'),
                                                    'admin_login_id'=>$this->request->getData('admin_login_id'),
                                                    'admin_password'=>$this->request->getData('admin_password'),
                                                    'admin_role'=>$this->request->getData('admin_role'),
                                                    ]);               
                $this->redirect(array('action' => 'confirm'));}
                else {
                    foreach ($errors as $key => $error) {
                        foreach ($error as $error_messages) {
                            $this->Flash->error($error_messages, ['key' => $key]);
                        }
                        unset($key, $error_messages);
                    }
                    unset($error);
                }
             }} 
                $this->set(compact('mAdmin'));
 
    }

    public function confirm()
    {
        $this->loadModel('MAdmins');
        $session = $this->getRequest()->getSession();
        $id = $session->read('id');
        $academy_id = $session->read('academy_id');
        $admin_name = $session->read('admin_name');
        $admin_login_id = $session->read('admin_login_id');
        $admin_password = $session->read('admin_password');
        $admin_role = $session->read('admin_role');
        
        $mAdmin = $this->MAdmins->newEmptyEntity();
        if ($this->request->is('post')) {
            $mAdmin = $this->MAdmins->patchEntity($mAdmin, $this->request->getData());
            $this->MAdmins->save($mAdmin);
            return $this->redirect(['action' => 'index']);
            $session->delete('id');
            $session->delete('academy_id');
            $session->delete('admin_name');
            $session->delete('admin_login_id');
            $session->delete('admin_password');
            $session->delete('admin_role');
        }
        $this->set(compact('mAdmin','id','academy_id','admin_name','admin_login_id','admin_password','admin_role'));
    }

    /**
     * Edit method
     *
     * @param string|null $id M Admin id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /*public function edit($id = null)
    {
        $mAdmin = $this->MAdmins->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mAdmin = $this->MAdmins->patchEntity($mAdmin, $this->request->getData());
            if ($this->MAdmins->save($mAdmin)) {
                $this->Flash->success(__('The m admin has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The m admin could not be saved. Please, try again.'));
        }
        $this->set(compact('mAdmin'));
    }*/

    public function delete($id = null)
    {   $this->loadModel('MAdmins'); 
        $MAdmin = $this->MAdmins->find()->where(['id' => $id])->first(); 
        $data = array(
            'is_deleted' => 1
        );
        $MAdmin=$this->MAdmins->patchEntity($MAdmin, $data);
        $this->MAdmins->save($MAdmin);
        return $this->redirect(['action' => 'index']); 
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['login','index','add','confirm','view','delete']);
    }

    public function login()
    {
    $this->viewBuilder()->setlayout('login');
    $result = $this->Authentication->getResult();
    if ($result->isValid()) {
        return $this->redirect(['action' => 'index']);
    }
    if ($this->request->is('post') && !$result->isValid()) {
        $this->Flash->error(__('ユーザー名またはパスワードが誤っています。'));
    }
    }

    public function logout()
    {
    $result = $this->Authentication->getResult();
    if ($result->isValid()) {
        $this->Authentication->logout();
    }
    return $this->redirect(['controller' => 'MAdmins', 'action' => 'login']);
    }
}
