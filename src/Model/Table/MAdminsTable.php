<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MAdmins Model
 *
 * @property \App\Model\Table\AdminLoginsTable&\Cake\ORM\Association\BelongsTo $AdminLogins
 * @property \App\Model\Table\AcademiesTable&\Cake\ORM\Association\BelongsTo $Academies
 *
 * @method \App\Model\Entity\MAdmin newEmptyEntity()
 * @method \App\Model\Entity\MAdmin newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\MAdmin[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MAdmin get($primaryKey, $options = [])
 * @method \App\Model\Entity\MAdmin findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\MAdmin patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MAdmin[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\MAdmin|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MAdmin saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MAdmin[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\MAdmin[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\MAdmin[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\MAdmin[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class MAdminsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('m_admins');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('admin_name')
            ->maxLength('admin_name', 50)
            ->requirePresence('admin_name', 'create')
            ->notEmptyString('admin_name')
            ->notEmpty('admin_name', '管理者名を入力してください。');
        
        $validator
            ->scalar('admin_login_id')
            ->maxLength('admin_login_id', 50)
            ->requirePresence('admin_login_id', 'create')
            ->notEmptyString('admin_login_id')
            ->notEmpty('admin_login_id', 'ログインIDを入力してください。');

        $validator
        ->notEmpty('admin_password', 'ログインパスワードを入力してください。')
        ->minLength('admin_password', 8, __('8文字以上を入力してください。'))  
        ->maxLength('admin_password', 20, __('20文字以下を入力してください。'))  
        ->add('admin_password', [  
            'inAlpha' => [  
                'rule' => ['custom', '/[a-zA-Z]+/'],  
                'message' => __('半角英字を1文字以上利用してください。'),  
            ],  
            'inNumber' => [  
                'rule' => ['custom', '/[0-9]+/'],  
                'message' => __('半角数字を1文字以上利用してください。'),  
            ],  
        ])    
        ;

        $validator
            ->requirePresence('admin_role', 'create')
            ->notEmptyString('admin_role')
            ->notEmpty('admin_role', '権限を選択してください。');
        $validator
            ->requirePresence('academy_id', 'create')
            ->notEmptyString('academy_id')
            ->notEmpty('academy_id', '学園を選択してください。');

        $validator
            ->boolean('is_deleted')
            ->notEmptyString('is_deleted');

        $validator
            ->scalar('created_by')
            ->maxLength('created_by', 50)
            ->allowEmptyString('created_by');

        $validator
            ->dateTime('created_at')
            ->allowEmptyDateTime('created_at');

        $validator
            ->scalar('updated_by')
            ->maxLength('updated_by', 50)
            ->allowEmptyString('updated_by');

        $validator
            ->dateTime('updated_at')
            ->allowEmptyDateTime('updated_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
}
