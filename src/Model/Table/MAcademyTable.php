<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MAcademy Model
 *
 * @method \App\Model\Entity\MAcademy newEmptyEntity()
 * @method \App\Model\Entity\MAcademy newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\MAcademy[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MAcademy get($primaryKey, $options = [])
 * @method \App\Model\Entity\MAcademy findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\MAcademy patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MAcademy[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\MAcademy|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MAcademy saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MAcademy[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\MAcademy[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\MAcademy[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\MAcademy[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class MAcademyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('m_academy');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('academy_code')
            ->maxLength('academy_code', 50)
            ->requirePresence('academy_code', 'create')
            ->notEmptyString('academy_code')
            ->add('academy_code', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('academy_name')
            ->maxLength('academy_name', 50)
            ->requirePresence('academy_name', 'create')
            ->notEmptyString('academy_name');

        $validator
            ->scalar('academy_email')
            ->maxLength('academy_email', 255)
            ->requirePresence('academy_email', 'create')
            ->notEmptyString('academy_email');

        $validator
            ->boolean('is_deleted')
            ->notEmptyString('is_deleted');

        $validator
            ->scalar('created_by')
            ->maxLength('created_by', 50)
            ->allowEmptyString('created_by');

        $validator
            ->dateTime('created_at')
            ->allowEmptyDateTime('created_at');

        $validator
            ->scalar('updated_by')
            ->maxLength('updated_by', 50)
            ->allowEmptyString('updated_by');

        $validator
            ->dateTime('updated_at')
            ->allowEmptyDateTime('updated_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['academy_code']), ['errorField' => 'academy_code']);

        return $rules;
    }
}
