<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MAcademy Entity
 *
 * @property int $id
 * @property string $academy_code
 * @property string $academy_name
 * @property string $academy_email
 * @property bool $is_deleted
 * @property string|null $created_by
 * @property \Cake\I18n\FrozenTime|null $created_at
 * @property string|null $updated_by
 * @property \Cake\I18n\FrozenTime|null $updated_at
 */
class MAcademy extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'academy_code' => true,
        'academy_name' => true,
        'academy_email' => true,
        'is_deleted' => true,
        'created_by' => true,
        'created_at' => true,
        'updated_by' => true,
        'updated_at' => true,
    ];
}
