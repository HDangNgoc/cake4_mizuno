<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MStudentsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MStudentsTable Test Case
 */
class MStudentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MStudentsTable
     */
    protected $MStudents;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.MStudents',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('MStudents') ? [] : ['className' => MStudentsTable::class];
        $this->MStudents = $this->getTableLocator()->get('MStudents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->MStudents);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\MStudentsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\MStudentsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
