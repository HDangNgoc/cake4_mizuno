<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MAcademyTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MAcademyTable Test Case
 */
class MAcademyTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MAcademyTable
     */
    protected $MAcademy;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.MAcademy',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('MAcademy') ? [] : ['className' => MAcademyTable::class];
        $this->MAcademy = $this->getTableLocator()->get('MAcademy', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->MAcademy);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\MAcademyTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\MAcademyTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
