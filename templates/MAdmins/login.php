<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
	<!--<meta name="viewport" content="width=device-width,initial-scale=1.0">-->
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="format-detection" content="telephone=no">
	<link rel="shortcut icon" href="/img/favicon.ico">
	<link rel="apple-touch-icon-precomposed" href="page_top画像のパス/apple-touch-icon-precomposed.png">
	<link rel="stylesheet" href="/css/base.css">
	<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="/js/function.js"></script>
  <title>学校法人 水野学園 学生WEBサイト管理画面</title>
</head>
<body id="admin" class="anim-box fadein is-animated">

<header>
	<div class="header-wrap">
		<div class="header-box">
				<div class="area01">
					<figure><a href="/index.html"><img src="/img/logo_main.png" alt="学校法人　水野学園"></a></figure>
					<div class="pg-ttl">学生WEBサイト管理画面</div>
				</div>
				
			
		</div>
	</div>
</header>
<div class="bg-gray">
<main>
	<section class="ttl">
		<h1>ログイン</h1>
	</section>
	
	<section class="content">
		<div class="login-form">
			<?= $this->Form->create() ?>
			<form action="/form.php" method="post">
            <input type="text" id="name" name="admin_login_id" placeholder="ユーザーID">
            <input type="password" id="password" name="admin_password" placeholder="パスワード">
        	<div class="btn-area">
				<button type="submit" class="btn top-back btnshine">ログイン</button>
			</div>
    </form>
			<?= $this->Form->end() ?>
		</div>
	</section>
		
</main>
</div>
<div id="page_top_admin" class="btnshine"><a href="#"></a></div>
<footer>
	<p>Copyright ＠ MizunoGakuen Ed.,Ltd. All Rights Reserved.</p>
</footer>
</body>
</html>
