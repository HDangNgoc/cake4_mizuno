<!DOCTYPE html>
<html>
<div class="bg-gray">
<main class="anim-box fadein is-animated">
	
	<section id="" class="sec02 information">
		<h1>管理者登録</h1>
		<?= $this->Form->create($mAdmin) ?>
		<div class="contact_form ch_pw">
			<?php echo $this->Form->input('id',['id'=>$mAdmin->id,'type'=>'hidden','value'=>$mAdmin->id]);?>
			<div style="margin-top: 4rem;">
				<label for="" class="must">学園名</label><br >
				<?php echo $this->Form->select('academy_id',
				[1=>'専門学校ヒコ･みづのジュエリーカレッジ',2=>'専門学校ヒコ・みづのジュエリーカレッジ大阪',3=>'東京サイクルデザイン専門学校',4=>'東京すし和食調理専門学校'],
				//['empty'=>'学園名を選択してください。'],
				['id'=>'academy_id','required' => false]);?>
				<?php echo $this->Flash->render('academy_id'); ?>
			</div>
			<div style="margin-top: 4rem;">
				<label for="" class="must">管理者名</label><br >
				<?php echo $this->Form->input('admin_name',['id'=>'admin_name','required' => false]); ?>
				<?php echo $this->Flash->render('admin_name'); ?>
			</div>
			<div style="margin-top: 4rem;">
				<label for="" class="must">ログインID</label><br >
				<?php echo $this->Form->input('admin_login_id',['id'=>'admin_login_id','required' => false]); ?>
				<?php echo $this->Flash->render('admin_login_id'); ?>			
			</div>
			<div style="margin-top: 4rem;">
				<label for="" class="must">ログインパスワード</label><br >
				<?php echo $this->Form->input('admin_password',['id'=>'admin_password','required' => false]); ?>
				<?php echo $this->Flash->render('admin_password'); ?>	
			</div>
			<div style="margin-top: 4rem;">
				<label for="" class="must">権限</label><br >
				<?php echo $this->Form->select('admin_role',
				[1=>'管理者',2=>'担当者'],['id'=>'admin_role','required' => false]); ?>
				<?php echo $this->Flash->render('admin_role'); ?>
			</div>
			<div class="btn-area">
			<div class="btn clear btnshine"><a href="" onclick="javascript:window.history.back(-1);return false;">戻る</a></div>
			<button type="submit" class="btn top-back btnshine">変更</button>			
			</div>
			<?= $this->Form->end() ?>
		</div>
		
	</section>

	
</main>
</div>
<div id="page_top_admin" class="btnshine"><a href="#"></a></div>
<footer>
	<p>Copyright ＠ MizunoGakuen Ed.,Ltd. All Rights Reserved.</p>
</footer>
</body>
</html>
