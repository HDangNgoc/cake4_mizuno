<!DOCTYPE html>
<html>
<div class="bg-gray">
<main class="anim-box fadein is-animated">
	
	<section id="" class="sec02 admin_list">
		<h1>管理者一覧</h1>
			<table class="admin_search">
			<tr>
				<th class="w50">学園</th>
				<th class="w35">氏名</th>
				<th class="w25"></th>
			</tr>
			<tr>
                <?= $this->Form->create(null,['type'=>'get'])?>
				<td>
				<?= $this->Form->select('academy_id',[''=>'選択してください',1=>'ヒコ・みづのジュエリーカレッジ',2=>'ヒコ・みづのジュエリーカレッジ　大阪校',3=>'東京サイクルデザイン専門学校',4=>'東京すし和食調理専門学校'],['default'=>$this->request->getQuery('academy_id')])?>
				</td>	
				<td><?= $this->Form->input('key',['label'=>'key','value'=>$this->request->getQuery('key')])?></td>
				
				<td>
                    <div class="btn-area">
                    <button class="btn top-back btnshine" type="submit">検索</button>
                    <a class="btn top-back btnshine" href="/m-admins/index">リセット</a>
					</div>
                </td>
                <?= $this->Form->end()?>
			</tr>
			</table>
			<div class="btn-area admin_register">
				<div class="btn top-back btnshine"><a href="/m-admins/add">新規登録</a></div>
			</div>
		<div class="search_result">
			<table>
			<tr>
				<th class="w15">No</th>
				<th class="w40">学園</th>
				<th class="w25">管理者名</th>
				<th class="w10"></th>
				<th class="w10"></th>
			</tr>
			<?php foreach ($query as $query): ?>
			<?php if($query->is_deleted != 1):?>
			<tr>
				<td><?= $this->Number->format($query->id) ?></td>
                <td><?= h($query->academy_name) ?></td>
                <td><?= h($query->admin_name) ?></td>
				<td style="padding: 0;"><span><a href="/m-admins/add/<?php echo $query->id ?>">編集</a></span></td>
				<td style="padding: 0;">
					<div id="dialog">
  					<p><?=$query->admin_name?>を削除します。本当によろしいですか？</p>
						<div class="dialog_btn">
						<button type="button" class="close_dialog">
    						いいえ
  						</button>
  						<button type="submit" class="submit close_dialog"> 
    						<a href="/m-admins/delete/<?php echo $query->id ?>"><input type="submit" value="はい"></a>
  						</button>
						</div>
					</div>
					<button type="button" class="open_dialog">
					  削除
					</button>
				</td>
			</tr>
			<?php endif;?>
			<?php endforeach; ?>
			</table>
		</div>
</div>

	</section>
	
	
	
</main>
</div>
<div id="page_top_admin" class="btnshine"><a href="#"></a></div>
<footer>
	<p>Copyright ＠ MizunoGakuen Ed.,Ltd. All Rights Reserved.</p>
</footer>
</body>
</html>
