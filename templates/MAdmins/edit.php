<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MAdmin $mAdmin
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $mAdmin->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $mAdmin->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List M Admins'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="mAdmins form content">
            <?= $this->Form->create($mAdmin) ?>
            <fieldset>
                <legend><?= __('Edit M Admin') ?></legend>
                <?php
                    echo $this->Form->control('admin_login_id');
                    echo $this->Form->control('admin_name');
                    echo $this->Form->control('admin_password');
                    echo $this->Form->control('academy_id');
                    echo $this->Form->control('admin_role');
                    echo $this->Form->control('is_deleted');
                    echo $this->Form->control('created_by');
                    echo $this->Form->control('created_at', ['empty' => true]);
                    echo $this->Form->control('updated_by');
                    echo $this->Form->control('updated_at', ['empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
