<!DOCTYPE html>
<html>
<div class="bg-gray">
<main class="anim-box fadein is-animated">
	
	<section id="" class="sec02 information">
		<h1>管理者登録</h1>
		<?= $this->Form->create($mAdmin) ?>
		<?php echo $this->Form->input('id',['value'=>$id,'type'=>'hidden']); ?>
		<div class="contact_form ch_pw">
			<div style="margin-top: 4rem;">
				<label for="" class="must">学園名</label><br >
				<?php if($academy_id == 1):?>
                <p>専門学校ヒコ･みづのジュエリーカレッジ</p>
            	<?php endif;?>
				<?php if($academy_id == 2):?>
                <p>ヒコ・みづのジュエリーカレッジ　大阪校</p>
            	<?php endif;?>
				<?php if($academy_id == 3):?>
                <p>東京サイクルデザイン専門学校</p>
            	<?php endif;?>
				<?php if($academy_id == 4):?>
                <p>東京すし和食調理専門学校</p>
            	<?php endif;?>
				<?php
				echo $this->Form->text('academy_id',['value'=>$academy_id, 'type'=>'hidden']);
				?>
			</div>
			<div style="margin-top: 4rem;">
				<label for="" class="must">管理者名</label><br >
				<?php echo $admin_name;
				echo $this->Form->text('admin_name',['value'=>$admin_name, 'type'=>'hidden']); ?>
			</div>
			<div style="margin-top: 4rem;">
				<label for="" class="must">ログインID</label><br >
				<?php  echo $admin_login_id;
				echo $this->Form->text('admin_login_id',['value'=>$admin_login_id, 'type'=>'hidden']); ?>
			</div>
			<div style="margin-top: 4rem;">
				<label for="" class="must">ログインパスワード</label><br >
				<?php echo $admin_password;
				echo $this->Form->text('admin_password',['value'=>$admin_password, 'type'=>'hidden', 'class'=>'password', 'id'=>'password']); ?>			
			</div>
			<div style="margin-top: 4rem;">
				<label for="" class="must">権限</label><br >
				<?php if($admin_role == 1):?>
                <p>管理者</p>
            	<?php else: ?>
                <p>担当者</p>
            	<?php endif;?>
            	<?php echo $this->Form->text('admin_role',['value'=>$admin_role, 'type'=>'hidden']);?>
			</div>
			<div class="btn-area">
			<div class="btn clear btnshine"><a href="" onclick="javascript:window.history.back(-1);return false;">戻る</a></div>
			<button type="submit" class="btn top-back btnshine">登録</button>
		<?= $this->Form->end() ?>
			</div>
		</div>
		
	</section>

	
</main>
</div>
<div id="page_top_admin" class="btnshine"><a href="#"></a></div>
<footer>
	<p>Copyright ＠ MizunoGakuen Ed.,Ltd. All Rights Reserved.</p>
</footer>
</body>
</html>
