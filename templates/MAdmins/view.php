<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4> 
            <?= $this->Html->link(__('List Users'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
        
    </aside>
    <div class="column-responsive column-80">
        <div class="users form content">
        <?= $this->Form->create($mAdmin) ?>
            <fieldset>
                <?php
                    echo $this->Form->control('admin_login_id');
                    echo $this->Form->control('admin_password');
                ?>
            </fieldset>   
            <?= $this->Form->button(__('Registry')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
