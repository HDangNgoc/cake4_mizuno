<!DOCTYPE html>
<html>
<div class="bg-gray">
<main class="anim-box fadein is-animated">
	
	<section class="sec02 information">
		<h1>学生情報編集</h1>
		<div class="bg_white">
			<table>
			
			<tr><th>学籍番号</th><td><?php echo $mStudent->student_no; ?></td></tr>
			<tr><th>氏名</th><td><?php echo $mStudent->student_name; ?></td></tr>
			<tr><th>所属</th><td><?php echo $mStudent->course_name; ?> <br> <?php echo $mStudent->department_name; ?></td></tr>
			<tr><th>学年</th><td><?php  echo $mStudent->grade; ?></td></tr>
			</table>
		</div>
	</section>
	
	<div class="line"></div>
	
	<section id="" class="sec02 information">
		<h2 class="no_img">本人連絡先</h2>
		<div class="bg_white">
			<table>
			<tr><th>住所</th><td><?php echo $mStudent->student_postcode; ?><br><?php echo $mStudent->student_prefecture; echo $mStudent->student_address; ?></td></tr>
			<tr><th>電話(携帯)</th><td>080-1234-9876</td></tr>
			<tr><th>メール</th><td><?php echo $mStudent->student_email; ?></td></tr>
			</table>
		</div>
			
		<div class="contact_form ch_pw">
			<?= $this->Form->create() ?>
			<div style="margin-top: 4rem;">
				<label for="" class="must">新しいパスワード</label><br >
				<input type="password" id="student_password" name="student_password" placeholder="">
				<?php echo $this->Flash->render('student_password'); ?>
			</div>
			<div style="margin-top: 2rem;">
				<label for="" class="must">パスワード(確認用)</label><br >
				<input type="password" id="parent_password" name="parent_password" placeholder="">
			</div>
			<div class="btn-area">
			<div class="btn clear btnshine"><a href="" onclick="javascript:window.history.back(-1);return false;">戻る</a></div>
			<button class="btn top-back btnshine"　type="submit">変更</button>
		</div>
			<?= $this->Form->end() ?>
		</div>
		
	</section>
	
</main>
</div>
<div id="page_top_admin" class="btnshine"><a href="#"></a></div>
<footer>
	<p>Copyright ＠ MizunoGakuen Ed.,Ltd. All Rights Reserved.</p>
</footer>
</body>
</html>
