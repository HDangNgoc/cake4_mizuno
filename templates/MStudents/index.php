<!DOCTYPE html>
<html>

<div class="bg-gray">
<main class="anim-box fadein is-animated">
	
	<section class="sec02">
		<h1>学生一覧</h1>
			<table class="seach">
			<tr>
				<th class="w15">学籍番号</th>
				<th class="w15">氏名</th>
				<th class="w15">ふりがな</th>
				<th class="w35">学園</th>
				<th class="w10">在籍区分</th>
			</tr>
			<tr>
				<?= $this->Form->create(null,['type'=>'get'])?>
				<td><?= $this->Form->input('student_no',['label'=>'学籍番号','value'=>$this->request->getQuery('student_no')])?></td>
        		<td><?= $this->Form->input('student_name',['label'=>'氏名','value'=>$this->request->getQuery('student_name')])?></td>
        		<td><?= $this->Form->input('student_name_phonetic',['label'=>'ふりがな','value'=>$this->request->getQuery('student_name_phonetic')])?></td>
				<td>
				<?= $this->Form->select('academy_name',[''=>'--','専門学校ヒコ･みづのジュエリーカレッジ'=>'専門学校ヒコ･みづのジュエリーカレッジ','ヒコ・みづのジュエリーカレッジ　大阪校'=>'ヒコ・みづのジュエリーカレッジ　大阪校','東京サイクルデザイン専門学校'=>'東京サイクルデザイン専門学校','東京すし和食調理専門学校'=>'東京すし和食調理専門学校'],['default'=>$this->request->getQuery('academy_name')])?>
				</td>
				<td>
				<?= $this->Form->select('enrollment_status',[''=>'--','在籍'=>'在籍','休学'=>'休学','復学'=>'復学','留年'=>'留年','編入'=>'編入','修了'=>'修了','停学'=>'停学'],['default'=>$this->request->getQuery('enrollment_status')])?>
				</td>
			</tr>
			</table>
			<div class="btn-area" style="text-align:center;">
				<button class="btn top-back btnshine" type="submit">検索</button>
				<button class="btn top-back btnshine" onclick="location.href='/m-students/index';" type="button">リセット</button>			
			</div>
			<?= $this->Form->end()?>
			
		<ul class="pagination" style="text-align:left">
            <?php echo $this->Paginator->prev('<<');?>
            <?php echo $this->Paginator->numbers(); ?>
            <?php echo $this->Paginator->next('>>');?>
            <p><?= $this->Paginator->counter(__('{{start}}～{{end}}件/{{count}}件')) ?></p>   
    	</ul>
			<table>
			<tr>
				<th class="w10">学籍番号</th>
				<th class="w10">氏名</th>
				<th class="w8" style="padding-left: 0;padding-right: 0">在籍区分</th>
				<th class="w5">学年</th>
				<th class="w5" style="padding-left: 0;padding-right: 0">クラス</th>
				<th class="w30">学園</th>
				<th class="w15">課程</th>
				<th class="w20">学科</th>
				<th class="w10"></th>
			</tr>
				<?php foreach ($mStudents as $mStudent): ?>
			<tr>
				<td><a  target="_blank"><?= h($mStudent->student_no) ?></a></td>
				<td><?= h($mStudent->student_name) ?></td>
				<td><?= h($mStudent->enrollment_status) ?></td>
				<td><?= $this->Number->format($mStudent->grade) ?></td>
				<td><?= h($mStudent->class) ?></td>
				<td style="padding-left: 0.5rem;padding-right: 0;text-align: left;"><?= h($mStudent->academy_name) ?></td>
				<td><?= h($mStudent->course_name) ?></td>
				<td style="padding-left: 0.5rem;padding-right: 0;text-align: left;"><?= h($mStudent->department_name) ?></td>
				<td style="padding: 0;"><span><a href="/m-students/edit/<?php echo $mStudent->id ?>" >編集</a></span></td>
			</tr>
				<?php endforeach?>
			</table>
		</div>
</div>

	</section>
	
	
	
</main>
</div>
<div id="page_top_admin" class="btnshine"><a href="#"></a></div>
<footer>
	<p>Copyright ＠ MizunoGakuen Ed.,Ltd. All Rights Reserved.</p>
</footer>
</body>
</html>
