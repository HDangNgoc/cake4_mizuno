<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MStudent $mStudent
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit M Student'), ['action' => 'edit', $mStudent->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete M Student'), ['action' => 'delete', $mStudent->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mStudent->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List M Students'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New M Student'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="mStudents view content">
            <h3><?= h($mStudent->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Student No') ?></th>
                    <td><?= h($mStudent->student_no) ?></td>
                </tr>
                <tr>
                    <th><?= __('Student Name') ?></th>
                    <td><?= h($mStudent->student_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Student Name Phonetic') ?></th>
                    <td><?= h($mStudent->student_name_phonetic) ?></td>
                </tr>
                <tr>
                    <th><?= __('Enrollment Status') ?></th>
                    <td><?= h($mStudent->enrollment_status) ?></td>
                </tr>
                <tr>
                    <th><?= __('Academy Name') ?></th>
                    <td><?= h($mStudent->academy_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Course Name') ?></th>
                    <td><?= h($mStudent->course_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Department Name') ?></th>
                    <td><?= h($mStudent->department_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Class') ?></th>
                    <td><?= h($mStudent->class) ?></td>
                </tr>
                <tr>
                    <th><?= __('Tuition Billing Destination') ?></th>
                    <td><?= h($mStudent->tuition_billing_destination) ?></td>
                </tr>
                <tr>
                    <th><?= __('Student Postcode') ?></th>
                    <td><?= h($mStudent->student_postcode) ?></td>
                </tr>
                <tr>
                    <th><?= __('Student Prefecture') ?></th>
                    <td><?= h($mStudent->student_prefecture) ?></td>
                </tr>
                <tr>
                    <th><?= __('Student Address') ?></th>
                    <td><?= h($mStudent->student_address) ?></td>
                </tr>
                <tr>
                    <th><?= __('Student Tel') ?></th>
                    <td><?= h($mStudent->student_tel) ?></td>
                </tr>
                <tr>
                    <th><?= __('Student Email') ?></th>
                    <td><?= h($mStudent->student_email) ?></td>
                </tr>
                <tr>
                    <th><?= __('Parent Name') ?></th>
                    <td><?= h($mStudent->parent_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Parent Postcode') ?></th>
                    <td><?= h($mStudent->parent_postcode) ?></td>
                </tr>
                <tr>
                    <th><?= __('Parent Prefecture') ?></th>
                    <td><?= h($mStudent->parent_prefecture) ?></td>
                </tr>
                <tr>
                    <th><?= __('Parent Address') ?></th>
                    <td><?= h($mStudent->parent_address) ?></td>
                </tr>
                <tr>
                    <th><?= __('Parent Tel') ?></th>
                    <td><?= h($mStudent->parent_tel) ?></td>
                </tr>
                <tr>
                    <th><?= __('Parent Email') ?></th>
                    <td><?= h($mStudent->parent_email) ?></td>
                </tr>
                <tr>
                    <th><?= __('Student Password') ?></th>
                    <td><?= h($mStudent->student_password) ?></td>
                </tr>
                <tr>
                    <th><?= __('Parent M Student') ?></th>
                    <td><?= $mStudent->has('parent_m_student') ? $this->Html->link($mStudent->parent_m_student->id, ['controller' => 'MStudents', 'action' => 'view', $mStudent->parent_m_student->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Parent Password') ?></th>
                    <td><?= h($mStudent->parent_password) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created By') ?></th>
                    <td><?= h($mStudent->created_by) ?></td>
                </tr>
                <tr>
                    <th><?= __('Updated By') ?></th>
                    <td><?= h($mStudent->updated_by) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($mStudent->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Grade') ?></th>
                    <td><?= $this->Number->format($mStudent->grade) ?></td>
                </tr>
                <tr>
                    <th><?= __('Student Birthday') ?></th>
                    <td><?= h($mStudent->student_birthday) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created At') ?></th>
                    <td><?= h($mStudent->created_at) ?></td>
                </tr>
                <tr>
                    <th><?= __('Updated At') ?></th>
                    <td><?= h($mStudent->updated_at) ?></td>
                </tr>
                <tr>
                    <th><?= __('Is Deleted') ?></th>
                    <td><?= $mStudent->is_deleted ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related M Students') ?></h4>
                <?php if (!empty($mStudent->child_m_students)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Student No') ?></th>
                            <th><?= __('Student Name') ?></th>
                            <th><?= __('Student Name Phonetic') ?></th>
                            <th><?= __('Student Birthday') ?></th>
                            <th><?= __('Enrollment Status') ?></th>
                            <th><?= __('Academy Name') ?></th>
                            <th><?= __('Course Name') ?></th>
                            <th><?= __('Department Name') ?></th>
                            <th><?= __('Grade') ?></th>
                            <th><?= __('Class') ?></th>
                            <th><?= __('Tuition Billing Destination') ?></th>
                            <th><?= __('Student Postcode') ?></th>
                            <th><?= __('Student Prefecture') ?></th>
                            <th><?= __('Student Address') ?></th>
                            <th><?= __('Student Tel') ?></th>
                            <th><?= __('Student Email') ?></th>
                            <th><?= __('Parent Name') ?></th>
                            <th><?= __('Parent Postcode') ?></th>
                            <th><?= __('Parent Prefecture') ?></th>
                            <th><?= __('Parent Address') ?></th>
                            <th><?= __('Parent Tel') ?></th>
                            <th><?= __('Parent Email') ?></th>
                            <th><?= __('Student Password') ?></th>
                            <th><?= __('Parent Id') ?></th>
                            <th><?= __('Parent Password') ?></th>
                            <th><?= __('Is Deleted') ?></th>
                            <th><?= __('Created By') ?></th>
                            <th><?= __('Created At') ?></th>
                            <th><?= __('Updated By') ?></th>
                            <th><?= __('Updated At') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($mStudent->child_m_students as $childMStudents) : ?>
                        <tr>
                            <td><?= h($childMStudents->id) ?></td>
                            <td><?= h($childMStudents->student_no) ?></td>
                            <td><?= h($childMStudents->student_name) ?></td>
                            <td><?= h($childMStudents->student_name_phonetic) ?></td>
                            <td><?= h($childMStudents->student_birthday) ?></td>
                            <td><?= h($childMStudents->enrollment_status) ?></td>
                            <td><?= h($childMStudents->academy_name) ?></td>
                            <td><?= h($childMStudents->course_name) ?></td>
                            <td><?= h($childMStudents->department_name) ?></td>
                            <td><?= h($childMStudents->grade) ?></td>
                            <td><?= h($childMStudents->class) ?></td>
                            <td><?= h($childMStudents->tuition_billing_destination) ?></td>
                            <td><?= h($childMStudents->student_postcode) ?></td>
                            <td><?= h($childMStudents->student_prefecture) ?></td>
                            <td><?= h($childMStudents->student_address) ?></td>
                            <td><?= h($childMStudents->student_tel) ?></td>
                            <td><?= h($childMStudents->student_email) ?></td>
                            <td><?= h($childMStudents->parent_name) ?></td>
                            <td><?= h($childMStudents->parent_postcode) ?></td>
                            <td><?= h($childMStudents->parent_prefecture) ?></td>
                            <td><?= h($childMStudents->parent_address) ?></td>
                            <td><?= h($childMStudents->parent_tel) ?></td>
                            <td><?= h($childMStudents->parent_email) ?></td>
                            <td><?= h($childMStudents->student_password) ?></td>
                            <td><?= h($childMStudents->parent_id) ?></td>
                            <td><?= h($childMStudents->parent_password) ?></td>
                            <td><?= h($childMStudents->is_deleted) ?></td>
                            <td><?= h($childMStudents->created_by) ?></td>
                            <td><?= h($childMStudents->created_at) ?></td>
                            <td><?= h($childMStudents->updated_by) ?></td>
                            <td><?= h($childMStudents->updated_at) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'MStudents', 'action' => 'view', $childMStudents->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'MStudents', 'action' => 'edit', $childMStudents->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'MStudents', 'action' => 'delete', $childMStudents->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childMStudents->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
