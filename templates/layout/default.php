<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

    <?= $this->Html->css(['top', 'base','bgswitcher','milligram.min','cake']) ?>
    <?php echo $this->Html->script(['function', 'jquery.bgswitcher.js', 'top']); ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

  <meta charset="utf-8">
	<!--<meta name="viewport" content="width=device-width,initial-scale=1.0">-->
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="format-detection" content="telephone=no">
	<link rel="shortcut icon" href="/img/favicon.ico">
	<link rel="apple-touch-icon-precomposed" href="画像のパス/apple-touch-icon-precomposed.png">
	<link rel="stylesheet" href="/css/base.css">
	<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="/js/function.js"></script>
  <title>学校法人 水野学園 | 学生WEBサイト管理画面 | 学生情報</title>

<body id="admin" class="admin2">

<header>
	<div class="header-wrap">
		<div class="header-box">
				<div class="area01">
					<figure><a href="/m-admins/index"><img src="/img/logo_main.png" alt="学校法人　水野学園"></a></figure>
					<div class="pg-ttl">学生WEBサイト管理画面</div>
				</div>
				<div class="area02">
					<div class="name"><?php /*echo $session->read('Madmins.admin_name');*/?> さん</div>
					<div class="logout"><a href="/m-admins/logout" style="">ログアウト</a></div>
				</div>
			<nav class="global">
				<ul>
					<li><a href="/m-students/index" class="nav_link01">学生一覧</a></li>
					<li><a href="/m-admins/index" class="nav_link02">管理者一覧</a></li>
				</ul>
			</nav>
		</div>
	</div>
</header>
</head>
<body>
    <main class="main">
        <div class="container">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
    </main>
    <footer>
    </footer>
</body>
</html>
