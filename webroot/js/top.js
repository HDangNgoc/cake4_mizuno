// JavaScript Document

//ログイン画面背景切り替え
jQuery(function($) {
		$('.bg-slider').bgSwitcher({
			images: ['/img/header-bg.png','/img/header-bg_02.png','/img/header-bg_03.png','/img/header-bg_04.png','/img/header-bg_05.png'], // 切り替える背景画像を指定
			interval: 6000, // 背景画像を切り替える間隔を指定 3000=3秒
		});
});

var timer = 0;
    var currentWidth = window.innerWidth;
	$(window).resize(function(){
        if (currentWidth == window.innerWidth) {
            return;
        }
        if (timer > 0) {
            clearTimeout(timer);
        }
 
        timer = setTimeout(function () {
            location.reload();
        }, 200);
		
	});


