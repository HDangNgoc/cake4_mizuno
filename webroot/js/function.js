// JavaScript Document

$(function() {
    $('.hamburger').click(function() {
        $(this).toggleClass('active');
 
        if ($(this).hasClass('active')) {
            $('.globalMenuSp').addClass('active');
        } else {
            $('.globalMenuSp').removeClass('active');
        }
    });
});

$(window).on('load scroll', function(){
  if ($(window).scrollTop() > 160) {
	$('.is_flow').fadeIn(400);
   } else {
	$('.is_flow').fadeOut(400);
   }
});

//TOPボタン
jQuery(function() {
    var pagetop = $('#page_top');   
    pagetop.hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {  //100pxスクロールしたら表示
            pagetop.fadeIn();
        } else {
            pagetop.fadeOut();
        }
    });
  $('a[href^="#"]').click(function(){
    var time = 500;
    var href= $(this).attr("href");
    var target = $(href == "#" ? 'html' : href);
    var distance = target.offset().top;
    $("html, body").animate({scrollTop:distance}, time, "swing");
    return false;
  });
});

//admin用TOPボタン
//TOPボタン
jQuery(function() {
    var pagetop = $('#page_top_admin');   
    pagetop.hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {  //100pxスクロールしたら表示
            pagetop.fadeIn();
        } else {
            pagetop.fadeOut();
        }
    });
  $('a[href^="#"]').click(function(){
    var time = 500;
    var href= $(this).attr("href");
    var target = $(href == "#" ? 'html' : href);
    var distance = target.offset().top;
    $("html, body").animate({scrollTop:distance}, time, "swing");
    return false;
  });
});

//問い合わせテキストエリア　クリアボタン
function clearTextarea() {
	var textareaForm = document.getElementById("comment");
  textareaForm.value = '';
}




/* オープン処理 */
$(document).on("click", ".open_dialog", function(){
  var dialog = $(this).prev();
  var leftPos = 0;
  var topPos = 0;
  dialog.css({"left": leftPos + "30%"});
  dialog.css({"top": topPos + "30vh"});
  dialog.show();
});

/* クローズ処理 */
$(document).on("click", ".close_dialog", function(){
  var dialog = $(this).parent().parent();
  dialog.hide();
});


